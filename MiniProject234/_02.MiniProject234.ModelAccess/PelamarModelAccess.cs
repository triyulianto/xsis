﻿using _03.MiniProject234.ModelView;
using _04.MiniProject234.ModelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.MiniProject234.ModelAccess
{
    public class PelamarModelAccess
    {
        public static string Message;
        public static List<PelamarModelView> GetListAll(string paramSearch, int order, Int32 paramPage, Int32 paramPageSize)
        {
            List<PelamarModelView> result = new List<PelamarModelView>();

            using (var db = new DB_SpecificationEntities())
            {
                var query = from a in db.vw_pelamar
                            select new PelamarModelView
                            {
                                id = a.id,
                                fullname = a.fullname,
                                nick_name = a.nick_name,
                                email = a.email,
                                phone_number1 = a.phone_number1,
                                phone_number2 = a.phone_number2,
                                major = a.major,
                                school_name = a.school_name
                            };
                query = query.Where(o => o.fullname.ToUpper().Trim().Contains(paramSearch.ToUpper()));


                if (order == 1)
                {
                    query = query.OrderBy(a => a.fullname);
                }
                else
                {
                    query = query.OrderByDescending(a => a.fullname);
                }
                result = query.Skip(paramPage).Take(paramPageSize).ToList();

            }
            return result;
        }
        public static PelamarModelView GetDetailsById(int paramId)
        {
            //bikin penampung datanya
            PelamarModelView result = new PelamarModelView();

            using (var db = new DB_SpecificationEntities())
            {

                var query = (from a in db.x_biodata
                             join b in db.x_riwayat_pendidikan
                                         on a.id equals b.biodata_id
                             join c in db.x_education_level
                                        on b.education_level_id equals c.id
                             orderby c.id descending
                             select new PelamarModelView
                             {
                                 id = a.id,
                                 created_by = a.created_by,
                                 created_on = a.created_on,
                                 modified_by = a.modified_by,
                                 modified_on = a.modified_on,
                                 deleted_by = a.deleted_by,
                                 deleted_on = a.deleted_on,
                                 is_delete = a.is_delete,
                                 fullname = a.fullname,
                                 nick_name = a.nick_name,
                                 pob = a.pob,
                                 dob = a.dob,
                                 gender = a.gender,
                                 religion_id = a.religion_id,
                                 high = a.high,
                                 weight = a.weight,
                                 nationality = a.nationality,
                                 ethnic = a.ethnic,
                                 hobby = a.hobby,
                                 identity_type_id = a.identity_type_id,
                                 identity_no = a.identity_no,
                                 email = a.email,
                                 phone_number1 = a.phone_number1,
                                 phone_number2 = a.phone_number2,
                                 parent_phone_number = a.parent_phone_number,
                                 child_sequence = a.child_sequence,
                                 how_many_brothers = a.how_many_brothers,
                                 marital_status_id = a.marital_status_id,
                                 addrbook_id = a.addrbook_id,
                                 token = a.token,
                                 expired_token = a.expired_token,
                                 marriage_year = a.marriage_year,
                                 company_id = a.company_id,
                                 is_process = a.is_process,
                                 is_complete = a.is_complete,
                                 id_lvl = c.id
                             });
                query = query.Where(o => o.id == paramId);
                result = query.FirstOrDefault();
            }
            return result;

        }
        public static Int32 GetCountData(String paramSearch)
        {
            Int32 countData = 0;
            using (var db = new DB_SpecificationEntities())
            {
                var query = from a in db.vw_pelamar
                            select new PelamarModelView
                            {
                                id = a.id,
                                fullname = a.fullname,
                                nick_name = a.nick_name,
                                email = a.email,
                                phone_number1 = a.phone_number1,
                                phone_number2 = a.phone_number2,
                                major = a.major,
                                school_name = a.school_name

                            };
                countData = (query.Where(o => o.fullname.ToUpper().Contains(paramSearch)).Count());
            }
                return countData;
        }
    }
}
