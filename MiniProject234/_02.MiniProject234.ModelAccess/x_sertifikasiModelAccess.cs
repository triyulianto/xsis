﻿using _03.MiniProject234.ModelView;
using _04.MiniProject234.ModelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.MiniProject234.ModelAccess
{
    public class x_sertifikasiModelAccess
    {
        public static string Message = string.Empty;
        public static List<x_sertifikasiModelView> GetListAll()
        {
            List<x_sertifikasiModelView> result = new List<x_sertifikasiModelView>();

            using (var db = new DB_SpecificationEntities())
            {
                result = (from a in db.x_sertifikasi
                          where a.is_delete == false
                            select new x_sertifikasiModelView
                            {
                                id = a.id,
                                created_by = a.created_by,
                                created_on = a.created_on,
                                modified_by = a.modified_by,
                                modified_on = a.modified_on,
                                deleted_by = a.deleted_by,
                                deleted_on = a.deleted_on,
                                is_delete = a.is_delete,
                                biodata_id = a.biodata_id,
                                certificate_name = a.certificate_name,
                                publisher = a.publisher,
                                valid_start_year = a.valid_start_year,
                                valid_start_month = a.valid_start_month,
                                until_year = a.until_year,
                                until_month = a.until_month,
                                notes = a.notes
                            }).ToList();
            }
            return result;
        }
        public static x_sertifikasiModelView GetDetailsByPelamar(int paramPelamar)
        {
            //buat penampung data
            x_sertifikasiModelView result = new x_sertifikasiModelView();

            using (var db = new DB_SpecificationEntities())
            {
                result = (from a in db.x_sertifikasi
                          join b in db.x_biodata
                            on a.biodata_id equals b.id
                          where a.biodata_id == paramPelamar
                          select new x_sertifikasiModelView
                          {
                              id = a.id,
                              created_by = a.created_by,
                              created_on = a.created_on,
                              modified_by = a.modified_by,
                              modified_on = a.modified_on,
                              deleted_by = a.deleted_by,
                              deleted_on = a.deleted_on,
                              is_delete = a.is_delete,
                              biodata_id = a.biodata_id,
                              certificate_name = a.certificate_name,
                              publisher = a.publisher,
                              valid_start_year = a.valid_start_year,
                              valid_start_month = a.valid_start_month,
                              until_year = a.until_year,
                              until_month = a.until_month,
                              notes = a.notes
                          }).FirstOrDefault();
                return result;
            }
        }
        public static x_sertifikasiModelView GetDetailsBy(int paramId)
        {
            //buat penampung data
            x_sertifikasiModelView result = new x_sertifikasiModelView();

            using (var db = new DB_SpecificationEntities())
            {
                result = (from a in db.x_sertifikasi
                          where a.id == paramId
                          select new x_sertifikasiModelView
                          {
                              id = a.id,
                              created_by = a.created_by,
                              created_on = a.created_on,
                              modified_by = a.modified_by,
                              modified_on = a.modified_on,
                              deleted_by = a.deleted_by,
                              deleted_on = a.deleted_on,
                              is_delete = a.is_delete,
                              biodata_id = a.biodata_id,
                              certificate_name = a.certificate_name,
                              publisher = a.publisher,
                              valid_start_year = a.valid_start_year,
                              valid_start_month = a.valid_start_month,
                              until_year = a.until_year,
                              until_month = a.until_month,
                              notes = a.notes
                          }).FirstOrDefault();
                return result;
            }
        }
        public static bool Insert(x_sertifikasiModelView paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_SpecificationEntities())
                {
                    x_sertifikasi a = new x_sertifikasi();
                    a.created_by = paramModel.created_by;
                    a.created_on = paramModel.created_on;
                    a.is_delete = paramModel.is_delete;
                    a.biodata_id = paramModel.biodata_id;
                    a.certificate_name = paramModel.certificate_name;
                    a.publisher = paramModel.publisher;
                    a.valid_start_year = paramModel.valid_start_year;
                    a.valid_start_month = paramModel.valid_start_month;
                    a.until_year = paramModel.until_year;
                    a.until_month = paramModel.until_month;
                    a.notes = paramModel.notes;
                    db.x_sertifikasi.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
        public static bool Update(x_sertifikasiModelView paraModel)
        {
            //penampung return
            bool result = true;

            try
            {
                using (var db = new DB_SpecificationEntities())
                {
                    //ambil data old
                    x_sertifikasi a = db.x_sertifikasi.Where(o => o.id == paraModel.id).FirstOrDefault();
                    if (a != null)
                    {
                        a.modified_by = paraModel.modified_by;
                        a.modified_on = paraModel.modified_on;
                        a.biodata_id = paraModel.biodata_id;
                        a.certificate_name = paraModel.certificate_name;
                        a.publisher = paraModel.publisher;
                        a.valid_start_year = paraModel.valid_start_year;
                        a.valid_start_month = paraModel.valid_start_month;
                        a.until_year = paraModel.until_year;
                        a.until_month = paraModel.until_month;
                        a.notes = paraModel.notes;

                        db.SaveChanges();
                        Message = "Data berhasil diubah!";
                    }
                    else
                    {
                        result = false;
                        Message = "Data tidak ditemukan!";
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
        public static bool Delete(x_sertifikasiModelView paraModel)
        {
            //variable penampung status
            bool result = true;
            try
            {
                using (var db = new DB_SpecificationEntities())
                {
                    //ambil data old
                    x_sertifikasi a = db.x_sertifikasi.Where(
                        o => o.id == paraModel.id
                        ).FirstOrDefault();
                    if (a != null)
                    {
                        a.is_delete = true;
                        a.deleted_by = paraModel.deleted_by;
                        a.deleted_on = paraModel.deleted_on;

                        db.SaveChanges();
                        Message = "Data berhasil diubah!";
                    }
                    else
                    {
                        result = false;
                        Message = "Data tidak ditemukan!";
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
    }
}
