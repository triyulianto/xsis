﻿using _03.MiniProject234.ModelView;
using _04.MiniProject234.ModelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.MiniProject234.ModelAccess
{
    public class KeluargaModelAccess
    {
        public static string Message = string.Empty;
        public static List<KeluargaModelView> GetListAll()
        {
            List<KeluargaModelView> result = new List<KeluargaModelView>();

            using (var db = new DB_SpecificationEntities())
            {
                result = (from a in db.x_keluarga
                          //join b in db.x_family_tree_type
                          //  on a.family_tree_type_id equals b.id
                          //join c in db.x_family_relation
                          where a.is_delete == false
                          select new KeluargaModelView
                          {
                              id = a.id,
                              createdBy = a.created_by,
                              createdOn = a.created_on,
                              modifiedBy = a.modified_by,
                              modifiedOn = a.modified_on,
                              deletedBy = a.deleted_by,
                              deletedOn = a.deleted_on,
                              isDelete = a.is_delete,
                              biodataId = a.biodata_id,
                              familyTreeTypeId = a.family_tree_type_id,
                              //nameTree = b.name, //Join Name Tree Family
                              familyRelationId = a.family_relation_id,
                              name = a.name,
                              gender = a.gender,
                              dob = a.dob,
                              educationLevelId = a.education_level_id,
                              job = a.job,
                              notes = a.notes
                          }).ToList();
            }
            return result;
        }

        //Get TreeType for Dropdown
        public static List<KeluargaModelView> GetTreeType()
        {
            List<KeluargaModelView> result = new List<KeluargaModelView>();

            using (var db = new DB_SpecificationEntities())
            {
                result = (from b in db.x_family_tree_type
                          select new KeluargaModelView
                          {
                              id = b.id,
                              nameTree = b.name
                          }).ToList();
            }
            return result;
        }

        public static KeluargaModelView GetDetailsBy(int paramId)
        {
            //buat penampung data
            KeluargaModelView result = new KeluargaModelView();

            using (var db = new DB_SpecificationEntities())
            {
                result = (from a in db.x_keluarga
                          where a.id == paramId
                          select new KeluargaModelView
                          {
                              id = a.id,
                              createdBy = a.created_by,
                              createdOn = a.created_on,
                              modifiedBy = a.modified_by,
                              modifiedOn = a.modified_on,
                              deletedBy = a.deleted_by,
                              deletedOn = a.deleted_on,
                              isDelete = a.is_delete,
                              biodataId = a.biodata_id,
                              familyTreeTypeId = a.family_tree_type_id,
                              familyRelationId = a.family_relation_id,
                              name = a.name,
                              gender = a.gender,
                              dob = a.dob,
                              educationLevelId = a.education_level_id,
                              job = a.job,
                              notes = a.notes
                          }).FirstOrDefault();
                return result;
            }
        }

        public static bool Insert(KeluargaModelView paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_SpecificationEntities())
                {
                    x_keluarga a = new x_keluarga();
                    a.created_by = paramModel.createdBy;
                    a.created_on = paramModel.createdOn;
                    a.is_delete = paramModel.isDelete;
                    a.biodata_id = paramModel.biodataId;
                    a.family_tree_type_id = paramModel.familyTreeTypeId;
                    a.family_relation_id = paramModel.familyRelationId;
                    a.name= paramModel.name;
                    a.gender = paramModel.gender;
                    a.dob = paramModel.dob;
                    a.education_level_id = paramModel.educationLevelId;
                    a.job = paramModel.job;
                    a.notes = paramModel.notes;
                    db.x_keluarga.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<FamilyRelationModelView> ByFamilyTree(long FamilyTypeId) // id adalah nama variable jadi bisa apasaja
        {
            List<FamilyRelationModelView> result = new List<FamilyRelationModelView>();
            using (var db = new DB_SpecificationEntities())
            {
                result = (from a in db.x_family_tree_type
                          join b in db.x_family_relation
                          on a.id equals b.family_tree_type_id
                          where a.id == FamilyTypeId
                          select new FamilyRelationModelView
                          {
                              idRelationType = b.id,
                              nameRelationType = b.name,
                              description = b.description
                          }).ToList();
            }
            return result;
        }

        #region NotYet

        //public static bool Update(KeluargaModelView paraModel)
        //{
        //    //penampung return
        //    bool result = true;

        //    try
        //    {
        //        using (var db = new DB_SpecificationEntities())
        //        {
        //            //ambil data old
        //            x_sertifikasi a = db.x_sertifikasi.Where(o => o.id == paraModel.id).FirstOrDefault();
        //            if (a != null)
        //            {
        //                a.modified_by = paraModel.modified_by;
        //                a.modified_on = paraModel.modified_on;
        //                a.biodata_id = paraModel.biodata_id;
        //                a.certificate_name = paraModel.certificate_name;
        //                a.publisher = paraModel.publisher;
        //                a.valid_start_year = paraModel.valid_start_year;
        //                a.valid_start_month = paraModel.valid_start_month;
        //                a.until_year = paraModel.until_year;
        //                a.until_month = paraModel.until_month;
        //                a.notes = paraModel.notes;

        //                db.SaveChanges();
        //                Message = "Data berhasil diubah!";
        //            }
        //            else
        //            {
        //                result = false;
        //                Message = "Data tidak ditemukan!";
        //            }
        //        }
        //    }
        //    catch (Exception hasError)
        //    {
        //        result = false;
        //        if (hasError.Message.ToLower().Contains("inner exception"))
        //        {
        //            Message = hasError.InnerException.InnerException.Message;
        //        }
        //        else
        //        {
        //            Message = hasError.Message;
        //        }
        //    }
        //    return result;
        //}
        //public static bool Delete(KeluargaModelView paraModel)
        //{
        //    //variable penampung status
        //    bool result = true;
        //    try
        //    {
        //        using (var db = new DB_SpecificationEntities())
        //        {
        //            //ambil data old
        //            x_sertifikasi a = db.x_sertifikasi.Where(
        //                o => o.id == paraModel.id
        //                ).FirstOrDefault();
        //            if (a != null)
        //            {
        //                a.is_delete = true;
        //                a.deleted_by = paraModel.deleted_by;
        //                a.deleted_on = paraModel.deleted_on;

        //                db.SaveChanges();
        //                Message = "Data berhasil diubah!";
        //            }
        //            else
        //            {
        //                result = false;
        //                Message = "Data tidak ditemukan!";
        //            }
        //        }
        //    }
        //    catch (Exception hasError)
        //    {
        //        result = false;
        //        if (hasError.Message.ToLower().Contains("inner exception"))
        //        {
        //            Message = hasError.InnerException.InnerException.Message;
        //        }
        //        else
        //        {
        //            Message = hasError.Message;
        //        }
        //    }
        //    return result;
        //}
        #endregion

    }
}
