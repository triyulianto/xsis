﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.MiniProject234.ModelView
{
    public class FamilyRelationModelView
    {
        public long idRelationType { get; set; }
        public string nameRelationType { get; set; }
        public string description { get; set; }
    }
}
