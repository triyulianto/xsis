﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.MiniProject234.ModelView
{
    public class RiwayatPendidikanModelView
    {
        public long id { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public Nullable<long> deleted_by { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public bool is_delete { get; set; }
        public long biodata_id { get; set; }
        public string school_name { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public Nullable<long> education_level_id { get; set; }
        public string entry_year { get; set; }
        public string graduation_year { get; set; }
        public string major { get; set; }
        public Nullable<double> gpa { get; set; }
        public string notes { get; set; }
        public Nullable<int> order { get; set; }
        public string judul_ta { get; set; }
        public string deskripsi_ta { get; set; }
    }
}
