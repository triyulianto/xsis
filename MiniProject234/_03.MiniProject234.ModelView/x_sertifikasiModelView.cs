﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _03.MiniProject234.ModelView
{
    public class x_sertifikasiModelView
    {
        public long id { get; set; }

        [DisplayName("Dibuat Oleh")]
        public long created_by { get; set; }

        [DisplayName("Tanggal Dibuat")]
        public System.DateTime created_on { get; set; }

        [DisplayName("Diubah Oleh")]
        [Range(1, 99999999999, ErrorMessage = "Data tidak sesuai! [maks.11 digit ID User]")]
        public Nullable<long> modified_by { get; set; }

        [DisplayName("Diubah Pada")]
        public Nullable<System.DateTime> modified_on { get; set; }

        [DisplayName("Dihapus Oleh")]
        [Range(1, 99999999999, ErrorMessage = "Data tidak sesuai! [maks.11 digit ID User]")]
        public Nullable<long> deleted_by { get; set; }

        [DisplayName("Dihapus Pada")]
        public Nullable<System.DateTime> deleted_on { get; set; }

        [DisplayName("Status")]
        public bool is_delete { get; set; }

        [DisplayName("ID Biodata")]
        [Range(1, 99999999999, ErrorMessage = "ID Biodata tidak sesuai! [maks.11 digit]")]
        public long biodata_id { get; set; }

        [DisplayName("Nama Sertifikasi")]
        [Required(ErrorMessage = "Nama Sertifikasi tidak boleh kosong!")]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "Nama sertifikasi tidak sesuai format! [max.200 karakter]")]
        public string certificate_name { get; set; }

        [DisplayName("Penyelenggara")]
        [Required(ErrorMessage = "Nama Penyelenggara tidak boleh kosong!")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Format penulisan Penyelenggara tidak sesuai! [max.100 karakter]")]
        public string publisher { get; set; }

        [DisplayName("Tahun Dibuat")]
        [StringLength(10, ErrorMessage = "Format penulisan tahun dibuat tidak sesuai")]
        public string valid_start_year { get; set; }

        [DisplayName("Dibuat Pada Bulan")]
        [StringLength(10, ErrorMessage = "Format penulisan tahun dibuat tidak sesuai")]
        public string valid_start_month { get; set; }

        [DisplayName("Tahun Akhir Berlaku")]
        [StringLength(10, ErrorMessage = "Format penulisan tahun dibuat tidak sesuai")]
        public string until_year { get; set; }
        
        [DisplayName("Berakhir Pada Bulan")]
        [StringLength(10, ErrorMessage = "Format penulisan bulan tidak sesuai")]
        public string until_month { get; set; }

        [DisplayName("Catatan")]
        [StringLength(1000, ErrorMessage = "Max.1000 karakter")]
        public string notes { get; set; }
                
    }
}
