﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.MiniProject234.ModelView
{
    public class FamilyTreeTypeModelView
    {
        public long idTreeType { get; set; }
        public string nameTreeTYpe { get; set; }
        public string description { get; set; }

    }
}
