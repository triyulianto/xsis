﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _03.MiniProject234.ModelView
{
    public class KeluargaModelView
    {
        public long id { get; set; }

        [DisplayName("Dibuat Oleh")]
        public long createdBy { get; set; }

        [DisplayName("Tanggal Dibuat")]
        public System.DateTime createdOn { get; set; }

        [DisplayName("Diubah Oleh")]
        [Range(1, 99999999999, ErrorMessage = "Data tidak sesuai! [maks.11 digit ID User]")]
        public Nullable<long> modifiedBy { get; set; }

        [DisplayName("Diubah Pada")]
        public Nullable<System.DateTime> modifiedOn { get; set; }

        [DisplayName("Dihapus Oleh")]
        [Range(1, 99999999999, ErrorMessage = "Data tidak sesuai! [maks.11 digit ID User]")]
        public Nullable<long> deletedBy { get; set; }

        [DisplayName("Dihapus Pada")]
        public Nullable<System.DateTime> deletedOn { get; set; }

        [DisplayName("Status")]
        public bool isDelete { get; set; }

        [DisplayName("ID Biodata")]
        [Range(1, 99999999999, ErrorMessage = "ID Biodata tidak sesuai! [maks.11 digit]")]
        public long biodataId { get; set; }

        [DisplayName("ID Tipe Keluarga")]
        [Range(1, 99999999999, ErrorMessage = "ID Biodata tidak sesuai! [maks.11 digit]")]
        public Nullable<long> familyTreeTypeId { get; set; }

        //Join Family Tree Name
        [DisplayName("Tipe Keluarga")]
        public string nameTree { get; set; }


        [DisplayName("Hubungan Keluarga")]
        [Range(1, 99999999999, ErrorMessage = "ID Biodata tidak sesuai! [maks.11 digit]")]
        public Nullable<long> familyRelationId { get; set; }

        [DisplayName("Tipe Keluarga")]
        public string nameRelation { get; set; }

        [DisplayName("Nama")]
        [StringLength(100, ErrorMessage = "Maks.100 karakter")]
        public string name { get; set; }

        [DisplayName("Jenis Kelamin")]
        public bool gender { get; set; }

        [DisplayName("Tanggal Lahir")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> dob { get; set; }

        [DisplayName("Pendidikan")]
        [Range(1, 99999999999, ErrorMessage = "ID Biodata tidak sesuai! [maks.11 digit]")]
        public Nullable<long> educationLevelId { get; set; }

        [DisplayName("Pekerjaan")]
        [StringLength(100, ErrorMessage = "Maks.100 karakter")]
        public string job { get; set; }

        [DisplayName("Catatan")]
        [StringLength(1000, ErrorMessage = "Max.1000 karakter")]
        public string notes { get; set; }

    }
}
