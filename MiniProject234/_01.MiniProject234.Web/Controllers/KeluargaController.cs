﻿using _01.MiniProject234.Web.Models.Shared;
using _02.MiniProject234.ModelAccess;
using _03.MiniProject234.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _01.MiniProject234.Web.Controllers
{
    public class KeluargaController : Controller
    {
        // GET: Keluarga
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult List()
        {
            return PartialView(KeluargaModelAccess.GetListAll());
        }

        //Create Controller

        public ActionResult GetByTreeType(long treeTypeId)
        {
            ViewBag.ListRelationTree = new SelectList(KeluargaModelAccess.ByFamilyTree(treeTypeId), "idRelationType", "nameRelationType");
            return View(KeluargaModelAccess.ByFamilyTree(treeTypeId));
        }

        public ActionResult Create()
        {
            ViewBag.ListFamilyTree = new SelectList(KeluargaModelAccess.GetTreeType(), "id", "nameTree");
            ViewBag.ListRelationTree = new SelectList(KeluargaModelAccess.ByFamilyTree(0), "idRelationType", "nameRelationType");
            return PartialView("Create", new KeluargaModelView());
        }
        [HttpPost]
        public ActionResult Create(KeluargaModelView paramModel)
        {
            try
            {
                paramModel.createdBy = 1;
                paramModel.createdOn = DateTime.Now;

                if (ModelState.IsValid)
                {
                    return Json(new
                    {
                        success = KeluargaModelAccess.Insert(paramModel),
                        message = KeluargaModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<Message> listMessage =
                     Message.GetMessageErrorByModel("paramModel", ModelState);

                    return Json(new
                    {
                        success = false,
                        message = listMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Undone



        //public ActionResult Edit(Int32 paramId)
        //{
        //    ViewBag.ListTahunMulai = new SelectList(Common.GetYear(), "TahunMulai", "TahunMulai");
        //    return PartialView(x_sertifikasiModelAccess.GetDetailsBy(paramId));
        //}

        //[HttpPost]
        //public ActionResult EditPost(x_sertifikasiModelView paraModel)
        //{
        //    try
        //    {
        //        paraModel.modified_by = 1;
        //        paraModel.modified_on = DateTime.Now;

        //        return Json(new
        //        {
        //            success = x_sertifikasiModelAccess.Update(paraModel),
        //            message = x_sertifikasiModelAccess.Message
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception hasError)
        //    {
        //        return Json(new
        //        {
        //            success = false,
        //            message = hasError.Message
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult Delete(Int32 paramId)
        //{
        //    return PartialView(x_sertifikasiModelAccess.GetDetailsBy(paramId));
        //}

        //[HttpPost]
        //public ActionResult Delete(x_sertifikasiModelView paraModel)
        //{
        //    try
        //    {
        //        paraModel.deleted_by = 1;
        //        paraModel.deleted_on = DateTime.Now;
        //        return Json(new
        //        {
        //            success = x_sertifikasiModelAccess.Delete(paraModel),
        //            message = x_sertifikasiModelAccess.Message
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception hasError)
        //    {
        //        return Json(new
        //        {
        //            success = false,
        //            message = hasError.Message
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        #endregion

    }
}