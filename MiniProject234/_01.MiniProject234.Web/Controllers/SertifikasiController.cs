﻿using _01.MiniProject234.Web.Models.Shared;
using _02.MiniProject234.ModelAccess;
using _03.MiniProject234.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _01.MiniProject234.Web.Controllers
{
    public class SertifikasiController : Controller
    {
        // GET: x_sertifikat
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int paramPelamar)
        {
            return PartialView(x_sertifikasiModelAccess.GetDetailsByPelamar(paramPelamar));
        }

        public ActionResult Create()
        {
            ViewBag.ListTahunMulai = new SelectList(Common.GetYear(), "TahunMulai", "TahunMulai");
            ViewBag.ListTahunAkhir = new SelectList(CommonEndYear.GetYear(), "TahunAkhir", "TahunAkhir");
            //ViewBag.ListTahunAkhir = new SelectList(Common.UntilYear(int start_year), "TahunMulai", "TahunMulai");
            //ViewBag.
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(x_sertifikasiModelView paramModel)
        {
            try
            {
                paramModel.created_by = 1;
                paramModel.created_on = DateTime.Now;

                if (ModelState.IsValid)
                {
                    return Json(new
                    {
                        success = x_sertifikasiModelAccess.Insert(paramModel),
                        message = x_sertifikasiModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<Message> listMessage =
                     Message.GetMessageErrorByModel("paramModel", ModelState);

                    return Json(new
                    {
                        success = false,
                        message = listMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(Int32 paramId)
        {
            ViewBag.ListTahunMulai = new SelectList(Common.GetYear(), "TahunMulai", "TahunMulai");
            return PartialView(x_sertifikasiModelAccess.GetDetailsBy(paramId));
        }

        [HttpPost]
        public ActionResult EditPost(x_sertifikasiModelView paraModel)
        {
            try
            {
                paraModel.modified_by = 1;
                paraModel.modified_on = DateTime.Now;
                if (ModelState.IsValid)
                {
                    return Json(new
                    {
                        success = x_sertifikasiModelAccess.Update(paraModel),
                        message = x_sertifikasiModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<Message> listMessage = Message.GetMessageErrorByModel("paraModel", ModelState);

                    return Json(new
                    {
                        success = false,
                        message = listMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(Int32 paramId)
        {
            return PartialView(x_sertifikasiModelAccess.GetDetailsBy(paramId));
        }

        [HttpPost]
        public ActionResult Delete(x_sertifikasiModelView paraModel)
        {
            try
            {
                paraModel.deleted_by = 1;
                paraModel.deleted_on = DateTime.Now;
                return Json(new
                {
                    success = x_sertifikasiModelAccess.Delete(paraModel),
                    message = x_sertifikasiModelAccess.Message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}