﻿using _01.MiniProject234.Web.Models;
using _02.MiniProject234.ModelAccess;
using _03.MiniProject234.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _01.MiniProject234.Web.Controllers
{
    public class PelamarController : Controller
    {
        // GET: Pelamar
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(string paramSearch, int order, int? paramPage, int paramPageSize)
        {
            List<PelamarModelView> list = new List<PelamarModelView>();

            Int32 countDataDb = PelamarModelAccess.GetCountData(paramSearch);

            Paging pg = new Paging(countDataDb, paramPage, paramPageSize);

            ViewData["pg"] = pg;

            int page = (pg.CurrentPage - 1) * pg.PageSize;
            list = PelamarModelAccess.GetListAll(paramSearch, order, page, paramPageSize);

            if (countDataDb == 0)
            {
                return PartialView("DataKosong", list);
            }
            else
            {
                return PartialView("List", list);
            }

        }
        public ActionResult None()
        {
            return View();
        }
        public ActionResult Detail(Int32 paramId)
        {
            return PartialView(PelamarModelAccess.GetDetailsById(paramId));
        }
        public ActionResult Profil(Int32 paramId)
        {
            return PartialView(PelamarModelAccess.GetDetailsById(paramId));
        }
    }
}