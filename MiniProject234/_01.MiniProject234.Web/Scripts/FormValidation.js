﻿function FormValidation(listError) {
    if (listError != null ||
        listError != "" ||
        listError != undefined
    ) {

        for (var i = 0; i < listError.length; i++) {
            if (document.getElementById(listError[i].PropertyName)) {
                ShowStyleDangerInput(listError[i].PropertyName,
                    listError[i].PropertyValue);
            }
        }
    }
}

function ShowStyleDangerInput(fieldName, errorText) {
    if (errorText != '') {
        $('#' + fieldName).css('border-color', 'red');
        $('#span_' + fieldName).html(errorText);
    }
    else {
        $('#' + fieldName).css('border-color', '');
        $('#span_' + fieldName).html('');
    }
}