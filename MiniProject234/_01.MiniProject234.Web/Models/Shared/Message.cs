﻿using _03.MiniProject234.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _01.MiniProject234.Web.Models.Shared
{
    public class Message
    {
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }

        public static List<Message> GetMessageErrorByModel(
            string modelName, ModelStateDictionary modelData)
        {
            List<Message> listError = new List<Message>();

            List<Message> listErrorByModel = GetMessageError(modelName, modelData);

            List<string> listFieldClass = typeof(x_sertifikasiModelView).GetProperties().Select(a => a.Name).ToList();

            foreach (var field in listFieldClass)
            {
                int index = listErrorByModel.FindIndex(n => n.PropertyName == field);

                Message message = new Message();
                message.PropertyName = field;

                if (index >= 0)
                {
                    message.PropertyValue = listErrorByModel[index].PropertyValue;
                }
                else { message.PropertyValue = string.Empty; }
                listError.Add(message);
            }
            return listError;
        }

        static List<Message> GetMessageError(string modelName,
            ModelStateDictionary modelData)
        {
            List<Message> listError = new List<Message>();
            foreach (KeyValuePair<string, ModelState>
                ms in modelData)
            {
                ModelState modelState = ms.Value;
                Message message = new Message();
                message.PropertyName =
                    GetFieldNameByObject(modelName + ".", ms.Key);

                if (modelState.Errors.Count > 0)
                {
                    message.PropertyValue =
                   string.Join("\n", GetErrorMessageByField(modelState));
                }
                else
                {
                    message.PropertyValue = string.Empty;
                }
                listError.Add(message);
            }
            return listError;
        }

        private static List<string> GetErrorMessageByField(
            ModelState modelState)
        {
            List<string> listError = new List<string>();
            foreach (ModelError error in modelState.Errors)
            {
                listError.Add(error.ErrorMessage);
            }

            return listError;
        }

        private static string GetFieldNameByObject(string v,
            string key)
        {
            if (key.Contains(v))
            {
                key = key.Replace(v, "");
            }
            return key;
        }
    }
}