﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01.MiniProject234.Web.Models.Shared
{
    public class Common
    {
        public int TahunMulai { get; set; }

        public static List<Common> GetYear()
        {
            DateTime tahun = DateTime.Now;
            List<Common> list = new List<Common>();
            for (int i = 0; i <= 20; i++)
            {
                Common c = new Common();
                c.TahunMulai = tahun.Year;
                list.Add(c);
                tahun = tahun.AddYears(-1);
            }
            return list;
        }

    }
    public class CommonEndYear
    {
        public int TahunAkhir { get; set; }

        public static List<CommonEndYear> GetYear()
        {
            DateTime tahun = DateTime.Now.AddYears(10);
            List<CommonEndYear> list = new List<CommonEndYear>();
            for (int i = 0; i <= 30; i++)
            {
                CommonEndYear c = new CommonEndYear();
                c.TahunAkhir = tahun.Year;
                list.Add(c);
                tahun = tahun.AddYears(-1);
            }
            return list;
        }

    }
}